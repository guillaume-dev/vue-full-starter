import Vue from 'vue';
import Router from 'vue-router';
import Resource from 'vue-resource';
import Auth from './modules/controllers/Auth';
// ROUTES
import App from './App.vue';
import Home from './modules/views/client/Home.vue';
import Login from './modules/views/auth/Login.vue';
import Projects from './modules/views/client/Projects.vue';
// ADMIN
import Admin from './modules/views/admin/Admin.vue';
import ProjectsAdmin from './modules/views/admin/Projects.vue';

// Use VueRouter
Vue.use(Router);
let router = new Router({
  hashbang: false,
  transitionOnLoad: true
});

// Use VueResource
Vue.use(Resource);
Vue.http.options.root = '/';
Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');

// Set routes here
router.map({
    '/': {
      name: 'home',
      component: Home,
      subRoutes: {
        '/projects': {
          name: 'projects',
          component: Projects
        }
      }
    },
    '/login': {
      name: 'login',
      component: Login
    },
    '/admin': {
      name: 'admin',
      component: Admin,
      subRoutes: {
        '/projects': {
          name: 'admin-projects',
          component: ProjectsAdmin
        }
      }
    }
});

router.beforeEach(function ({ to, next }) {
  if (to.path.match(/admin/gi) ) {

    let loggedIn = Auth.checkAuth();

    // if (!loggedIn) {
    //   router.go({ path: 'login' });
    // } else {
      next();
    // }
  } else {
    next();
  }
})

Auth.Router = router;

Auth.checkAuth();

// Start app with default Vue class
router.start(App, "#app");
