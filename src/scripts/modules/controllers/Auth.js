// src/auth/index.js

import Config from '../helpers/Config';

// URL and endpoint constants
const API_URL = Config.API_URL;
const LOGIN_URL = API_URL + '/sessions/create/';
const SIGNUP_URL = API_URL + '/users/';

export default {

  Router: null,

  // User object will let us check authentication status
  user: {
    authenticated: localStorage.getItem('id_token') ? true : false
  },

  // Send a request to the login URL and save the returned JWT
  login(context, creds, redirect) {
    context.$http.post(LOGIN_URL, creds, (data) => {
      localStorage.setItem('id_token', data.id_token);

      this.user.authenticated = true;

      // Redirect to a specified route
      if(redirect) {
        this.Router.go(redirect);
      }

    }).error((err) => {
      context.error = err;
    })
  },

  signup(context, creds, redirect) {
    context.$http.post(SIGNUP_URL, creds, (data) => {
      localStorage.setItem('id_token', data.id_token);

      this.user.authenticated = true;

      if(redirect) {
        this.Router.go(redirect);
      }

    }).error((err) => {
      context.error = err;
    })
  },

  // To log out, we just need to remove the token
  logout() {
    localStorage.removeItem('id_token');
    this.user.authenticated = false;
  },

  checkAuth() {
    var jwt = localStorage.getItem('id_token');
    if(jwt) {
      this.user.authenticated = true;
    }
    else {
      this.user.authenticated = false;
    }

    return this.user.authenticated;
  },

  // The object to be passed as a header for authenticated requests
  getAuthHeader() {
    return {
      'Authorization': 'Bearer ' + localStorage.getItem('id_token')
    }
  }
}
