export default class Utils {

  constructor() {

  }

  randomRange( min, max ) {

    return Math.floor( min + Math.random() * ( max - min ) );

  }

  clone( object ){

    return JSON.parse( JSON.stringify( object ) );

  }

  loadImageAsync(url) {
    return new Promise(function(resolve, reject) {
      let image = new Image();

      image.onload = function() {
        resolve(image);
      };

      image.onerror = function() {
        reject(new Error('Could not load image at ' + url));
      };

      image.src = url;
    });
  }

  loadImage(url, callback) {
    let image = new Image();
    image.onload = function() {
      callback(null, image);
    };
    image.onerror = function() {
      callback(new Error('Could not load image at ' + url));
    };
    image.src = url;
  }

  loadImages(urls, callback) {
    let returned = false;
    let count = 0;
    let result = new Array(urls.length);
    urls.forEach(function(url, index) {
      this.loadImage(url, function(error, item) {
        if (returned) return;
        if (error) {
          returned = true;
          return callback(error);
        }
        result[index] = item;
        count++;
        if (count === urls.length) {
          return callback(null, result);
        }
      });
    });
  }
}
